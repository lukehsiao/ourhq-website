+++
title = "OurHQ"
sort_by = "weight"
+++

# OurHQ: A inclusive online community for youth

**OurHQ** is a youth-focused online community based in Palo Alto and started by the Palo Alto youth of the Church of Jesus Christ of Latter-Day Saints.
We connect youth and mentors together so we can:

- Explore and learn about a variety of interests and skills
- Socialize and meet new people with diverse backgrounds
- Provide grassroots leadership and service opportunities
- Have a good time!

Being an online community, we're note restricted to discrete blocks of time to meet together.
Our members organically come and go throughout the week.
So, if you're working on something and want some company, or are just bored and want to see what's happening, feel free to pop in!

# Special Events

We also have regularly scheduled events where we can gather together as a community to enjoy a new project, listen to a talk, play some games, and/or discuss new ideas.
You can find when these events will be happening on the calendar below.
If you're interested in getting involved in one of these events, [let us know][email]!

<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23fdf6e3&amp;ctz=America%2FLos_Angeles&amp;src=ZWczMXY1c3NxMDg3aGRqb2E1Zzc3NXNyNWdAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=ZW4udXNhI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23795548&amp;color=%23009688&amp;mode=MONTH&amp;showCalendars=1&amp;showNav=1&amp;showTitle=1&amp;title=OurHQ%20Schedule" width="100%" height="600" frameborder="0" scrolling="no"></iframe>

[email]: mailto:ourhq@hsiao.dev
