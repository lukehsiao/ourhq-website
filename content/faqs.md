+++
title = "FAQs"
description = "Frequently Asked Questions"
weight = 2
+++

# Who can join?
Anyone is encouraged to request an invitation.
If you already have a connection to our community, you'll be instantly approved.
OurHQ is a place for everyone, people from all backgrounds and interests.
Everyone is welcome.
You can request an invitation by filling out [this form].

# Can I invite my friends?
Yes, please do! You can generate invite codes in Discord by following [this guide][invites].

# Can you host a scheduled event for me?
We enjoy highlighting a variety interests. If you have a project you're
working on, a talk you want to give, or a prototype you want feedback on, we'd
love to have you! Contact us [via email][email],  and we'll work with you
to schedule a featured appearance.

# What is expected from me?
This is a no-strings-attached place to hang out 🙂.
That said, we encourage you to get involved by sharing some of your talents and connecting with the other youth and mentors available here.
We expect all of our community members to follow our [Code of Conduct](@/code_of_conduct.md).

# Why use Discord?
We wanted a user-friendly community space to hang out in during this pandemic.
We like the flexibility it gives us to hop around different channels, voice chat, screen share, listen to music together, share files, etc.
It mimics the serendipity of a physical community space, while still conforming to safety guidelines.

[email]: mailto:ourhq@hsiao.dev
[invites]: https://support.discord.com/hc/en-us/articles/204155938-How-do-I-invite-friends-to-my-server-
